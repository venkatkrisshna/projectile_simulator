! ====================== !
!  PROJECTILE SIMULATOR  !
! ====================== !
! Venkata Krisshna

! Simulate the motion of a projectile object
! Equipped with generation of a SILO output
! file that can be visualized on VisIt

program projectile_simulator

  implicit none
  include 'silo_f9x.inc'

  ! Precision
  integer, parameter :: SP = kind(1.0)
  integer, parameter :: WP = kind(1.0d0)

  ! Scientific constants
  real(WP),parameter :: pi=3.141592653589793238_WP
  real(WP),parameter :: g=9.82182_WP ! Local (Bozeman,MT)

  ! Computational grid objects
  real(WP) :: Lx,Ly,Lz
  integer  :: nx,ny,nz
  real(WP) :: dx,dy,dz
  integer :: imin ,imax ,jmin ,jmax, kmin, kmax
  integer :: imino,imaxo,jmino,jmaxo,kmino,kmaxo
  integer :: ng=1
  integer :: ntime,nt_end=100
  real(WP) :: time,dt,t_end=1.0_WP
  real(WP),dimension(:),allocatable :: x ,y ,z
  real(WP),dimension(:),allocatable :: xm,ym,zm
  integer,dimension(:,:,:),allocatable :: projectile
  integer, dimension(:,:,:,:), allocatable :: c_i
  
  ! Problem physics objects
  real(WP) :: mass,v_init,a_init
  real(WP) :: x_init,y_init,z_init
  real(WP) :: theta,theta_rad
  real(WP) :: omega
  real(WP) :: pro_x,pro_y,pro_z
  real(WP) :: max_pro_x,max_pro_y
  real(WP) :: f_x,f_y,f_z
  
  print*,''
  print*,'===================================================='
  print*,'         Welcome to the Projectile Simulator        '
  print*,'===================================================='
  print*,'              Author: Venkata Krisshna              '
  print*,''
  
  call init
  call step
  
contains

  ! ======================= !
  !  Initialize simulation  !
  ! ======================= !
  subroutine init
    implicit none

    call sim_init
    call variables_init
    call grid_init
    call silo_init
    call pro_init
    call monitor_init
    
    return
  end subroutine init
  
  ! ============= !
  !  Main solver  !
  ! ============= !
  subroutine step
    implicit none
    
    print*,'Simulator Running!'

    main: do ntime = 1,nt_end+1
       ! Increment time
       time = time + dt
       ! Terminate for step limit
       if (ntime.eq.nt_end) then
          call terminate_simulator('ntime')
          exit main
       end if
       ! Terminate for simulation time limit
       if (time.gt.t_end) then
          call terminate_simulator('time')
          exit main
       end if
       ! Perform calculations
       call projectile_step
       ! Terminate for out of bounds
       if (&
            pro_x.lt.x(imin) .or. pro_x.gt.x(imax+1) .or. &
            pro_y.lt.y(jmin) .or. pro_y.gt.y(jmax+1) .or. &
            pro_z.lt.z(kmin) .or. pro_z.gt.z(kmax+1)) then
          call terminate_simulator('bound')
          exit main
       end if
       ! Post process for this step
       call projectile_locate(pro_x,pro_y,pro_z)
       call monitor_step
       call silo_step
    end do main
    
    return
  end subroutine step

  ! ============ !
  !  Read input  !
  ! ============ !
  subroutine sim_init
    implicit none
    
    print*,'Simulator Initializing!'

    ! Problem Physics
    mass = 1.0_WP       ! Mass of projectile (kg)
    v_init = 1.3_WP     ! Initial velocity (m/s)
    a_init = 0.0_WP     ! Initial acceleration (m^2/s)
    theta = 50.0_WP     ! Projectile angle (degrees)
    omega = 0.0_WP      ! Ambient angular velocity (rad/s)
    f_x = 0.0_WP        ! x-component of inital force (N)
    f_y = 0.0_WP        ! y-component of inital force (N)
    f_z = 0.0_WP        ! z-component of inital force (N)
    
    ! Computational Parameters
    nx = 200            ! Grid points in x
    ny = 200            ! Grid points in y
    nz = 22             ! Grid points in z
    Lx = 1.0_WP         ! Length of domain in x (m)
    Ly = 1.0_WP         ! Length of domain in y (m)
    Lz = 0.1_WP         ! Length of domain in z (m)
    x_init = 0.0_WP     ! Initial position of projectile in x (m)
    y_init = 0.0_WP     ! Initial position of projectile in y (m)
    z_init = 0.05_WP    ! Initial position of projectile in z (m)
    dt = 0.01_WP        ! Timestep (s)
    t_end = 10.0_WP     ! End simulation after time (s)
    nt_end = 100        ! End simulation after number of steps

    return
  end subroutine sim_init

  ! ====================== !
  !  Initialize variables  !
  ! ====================== !
  subroutine variables_init
    implicit none

    theta_rad = theta*pi/180.0_WP
    time = 0.0_WP
    ntime=0
    
    return
  end subroutine variables_init

  ! =============== !
  !  Initiate grid  !
  ! =============== !
  subroutine grid_init
    implicit none
    integer :: i,j,k
    
    ! Index extents
    imin=1; imax=imin+nx-1
    jmin=1; jmax=jmin+ny-1
    kmin=1; kmax=kmin+nz-1

    ! Ghost indices
    imino=imin-ng; imaxo=imax+ng
    jmino=jmin-ng; jmaxo=jmax+ng
    kmino=kmin-ng; kmaxo=kmax+ng
    
    ! Grid spacing
    dx = Lx/real(nx,WP)
    dy = Ly/real(ny,WP)
    dz = Lz/real(nz,WP)
    
    ! Allocate arrays
    allocate(x(imino:imaxo+1))
    allocate(y(jmino:jmaxo+1))
    allocate(z(kmino:kmaxo+1))
    allocate(xm(imino:imaxo))
    allocate(ym(jmino:jmaxo))
    allocate(zm(kmino:kmaxo))

    ! Create grid
    do i=imino,imaxo+1
       x(i) = real(i-1,WP)*dx
       if (i.ne.imaxo+1) xm(i) = 0.5_WP*(x(i)+x(i+1))
    end do
    do j=jmino,jmaxo+1
       y(j) = real(j-1,WP)*dy
       if (j.ne.jmaxo+1) ym(j) = 0.5_WP*(y(j)+y(j+1))
    end do
    do k=kmino,kmaxo+1
       z(k) = real(k-1,WP)*dz
       if (k.ne.kmaxo+1) zm(k) = 0.5_WP*(z(k)+z(k+1))
    end do

    return
  end subroutine grid_init

  ! =============== !
  !  Initiate SILO  !
  ! =============== !
  subroutine silo_init
    implicit none
    
    call execute_command_line('rm Visit/step* *.visit') ! Erase existing Visit data
    call execute_command_line('mkdir -p Visit') ! Create new directory for Visit data
    allocate(c_i(imin:imax,jmin:jmax,kmin:kmax,1:3))
    call silo_cellindex
    
    return
  end subroutine silo_init
  
  ! =============================== !
  !  Place bob at initial position  !
  ! =============================== !
  subroutine pro_init
    implicit none
    
    ! Allocate object
    allocate(projectile(imino:imaxo,jmino:jmaxo,kmino:kmaxo))
    projectile = 0

    ! Initial position
    pro_x = x_init
    pro_y = y_init
    pro_z = z_init
    call projectile_locate(pro_x,pro_y,pro_z)

    ! Write to SILO
    call silo_step

    ! Set max values to zero
    max_pro_x = 0.0_WP
    max_pro_y = 0.0_WP
    
    return
  end subroutine pro_init

  ! ========================= !
  !  Initialize montior file  !
  ! ========================= !
  subroutine monitor_init
    implicit none
    integer :: file_log
    
    ! Create the directory
    call execute_command_line('mkdir -p monitor') ! Create new directory for monitor

    ! Open log file
    file_log = 21
    open(file_log,file="monitor/log", form="formatted",status="REPLACE")

    return
  end subroutine monitor_init

  ! ====================== !
  !  Projectile iteration  !
  ! ====================== !
  subroutine projectile_step
    implicit none
    
    ! Governing equation
    pro_x = v_init*time*cos(theta_rad)
    pro_y = v_init*time*sin(theta_rad) - 0.5_WP*g*time**2
    pro_z = pro_z

    if (&
         pro_x.ge.x(imin) .and. pro_x.le.x(imax+1) .and. &
         pro_y.ge.y(jmin) .and. pro_y.le.y(jmax+1) .and. &
         pro_z.ge.z(kmin) .and. pro_z.le.z(kmax+1)) then
       max_pro_x = max(pro_x,max_pro_x)
       max_pro_y = max(pro_y,max_pro_y)
    end if
    
    return
  end subroutine projectile_step

  ! ================== !
  !  Place Projectile  !
  ! ================== !
  subroutine projectile_locate(x,y,z)
    implicit none
    real(WP),intent(in) :: x,y,z
    integer :: i,j,k
    
    ! Locate projectile indices
    projectile = 0
    do i = imino,imax
       if (xm(i+1).gt.x) exit
    end do
    do j = jmino,jmax
       if (ym(j+1).gt.y) exit
    end do
    do k = kmino,kmax
       if (zm(k+1).gt.z) exit
    end do
    
    ! Place projectile
    projectile(i,j,k) = 1
    
    return
  end subroutine projectile_locate
  
  ! ==================== !
  !  Write to SILO file  !
  ! ==================== !
  subroutine silo_step
    implicit none
    integer :: dbfile, ierr
    character(len=50) :: siloname
    
    write(siloname,'(A,I0.3,A)') 'Visit/step',ntime,'.silo'
    ierr = dbcreate(siloname, len_trim(siloname), DB_CLOBBER, DB_LOCAL,"Silo database", 13, DB_HDF5, dbfile)
    if(dbfile.eq.-1) print *,'Could not create Silo file!'
    ierr = dbclose(dbfile)
    ierr = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)
    ierr = dbputqm(dbfile,"Grid",4,'xc',2,'yc',2,'zc',2,x,y,z,&
         (/nx+1,ny+1,nz+1/),3,DB_DOUBLE,DB_COLLINEAR,DB_F77NULL,ierr)
    ierr = dbputqv1(dbfile,'Projectile',10,"Grid",4,projectile(imin:imax,jmin:jmax,kmin:kmax),&
         (/nx,ny,nz/),3,DB_F77NULL,0,DB_INT,DB_ZONECENT,DB_F77NULL,ierr)
    ierr = dbputqv1(dbfile,'i',1,"Grid",4,c_i(:,:,:,1),&
         (/nx,ny,nz/),3,DB_F77NULL,0,DB_INT,DB_ZONECENT,DB_F77NULL,ierr)
    ierr = dbputqv1(dbfile,'j',1,"Grid",4,c_i(:,:,:,2),&
         (/nx,ny,nz/),3,DB_F77NULL,0,DB_INT,DB_ZONECENT,DB_F77NULL,ierr)
    ierr = dbputqv1(dbfile,'k',1,"Grid",4,c_i(:,:,:,3),&
         (/nx,ny,nz/),3,DB_F77NULL,0,DB_INT,DB_ZONECENT,DB_F77NULL,ierr)
    ierr = dbclose(dbfile)

    open (21, file='sim.visit',position = 'append')
    write(21,'(A,I0.3,A)') 'Visit/step',ntime,'.silo'
    close(21)

    return
  end subroutine silo_step
  
  ! ============================ !
  !  Create cell index for mesh  !
  ! ============================ !
  subroutine silo_cellindex
    implicit none
    integer :: i,j,k

    c_i = 0
    do k = kmin,kmax
       do j = jmin,jmax
          do i = imin,imax
             c_i(i,j,k,1) = i
             c_i(i,j,k,2) = j
             c_i(i,j,k,3) = k
          end do
       end do
    end do

    return
  end subroutine silo_cellindex

  ! ======================= !
  !  Write to montior file  !
  ! ======================= !
  subroutine monitor_step
    implicit none
    
    return
  end subroutine monitor_step

  ! ================== !
  !  Terminate Solver  !
  ! ================== !
  subroutine terminate_simulator(term)
    implicit none
    character(len=*),intent(in) :: term
    real(WP) :: an_t,an_h,an_r
    real(WP) :: err_t,err_h,err_r

    an_t = real(2.0_WP*v_init*sin(theta_rad)/g,SP)
    an_h = real(sin(theta_rad)**2*v_init**2*0.5_WP/g,SP)
    an_r = real(sin(2.0_WP*theta_rad)*v_init**2/g,SP)

    ! Percentage error
    err_t = abs(an_t-time)*100.0_WP/an_t
    err_h = abs(an_h-max_pro_y)*100.0_WP/an_h
    err_r = abs(an_r-max_pro_x)*100.0_WP/an_r
    
    select case(trim(term))
    case('time') ; print*,'Simulator terminated after reaching run time limit!'
    case('ntime'); print*,'Simulator terminated after reaching step limit!'
    case('bound')
       print*,'Simulator terminated since projectile is out of bounds!'
       if (pro_x.lt.x(imin))   print*,'Projectile exited left boundary!'
       if (pro_y.lt.y(jmin))   print*,'Projectile exited bottom boundary!'
       if (pro_z.lt.z(kmin))   print*,'Projectile exited rear boundary!'
       if (pro_x.gt.x(imax+1)) print*,'Projectile exited right boundary!'
       if (pro_y.gt.y(jmax+1)) print*,'Projectile exited top boundary!'
       if (pro_z.gt.z(kmax+1)) print*,'Projectile exited front boundary!'
    end select
    print*,'Number of steps = ',ntime
    print*,'Simulation time = ',real(time,SP),'s'
    print*,'-----------------------------------------------------------------------------'

    print*,'     Data           |  Analytical Sol   |   Simulation Res   |    Error (%)'
    print*,'-----------------------------------------------------------------------------'
    print*,'Time of Flight (s)  | ',real(an_t,SP),'|  ',real(time,SP),'|',real(err_t,SP)
    print*,'Maximum Height (m)  | ',real(an_h,SP),'|  ',real(max_pro_y,SP),'|',real(err_h,SP)
    print*,'Maximum Range  (m)  | ',real(an_r,SP),'|  ',real(max_pro_x,SP),'|',real(err_r,SP)
    print*,''
    print*,'================= End of Simulator ================='
    print*,''
    
    return
  end subroutine terminate_simulator
  
end program projectile_simulator
